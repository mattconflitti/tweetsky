#TweetSky
##Filling the sky with Twitter Word Clouds

###This React-Native Application was created for an assignment in CIS350.

####Created by Danny DeRuiter, David Calkins, and Matt Conflitti

###ESDOC         : /doc/index.html
###ESLINT        : /style_report.html
###Code Coverage : /coverage/lcov-report/index.html