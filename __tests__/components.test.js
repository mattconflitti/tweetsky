import React from 'react';
import 'react-native';
//import { SearchBar } from '../src/components';
import { Button, Input } from '../src/components/common';
import renderer from 'react-test-renderer';

it('button renders correctly with no props', () => {
    const tree = renderer.create(
        <Button>Hello World</Button>
    ).toJSON();
    expect(tree).toMatchSnapshot();
});

it('input renders correctly with no props', () => {
    const tree = renderer.create(
        <Input />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});

// it('searchbar renders correctly with dummy suggestions', () => {
//     const tree = renderer.create(
//         <SearchBar
//             style={{}}
//             getSuggestions={jest.fn}
//             searchValue="testSearch"
//             suggestionData={[{ name: 'test1' }, { name: 'test2' }, {name: 'test3'}]}
//         />
//     ).toJSON();
//     expect(tree).toMatchSnapshot();
// });
//
// it('searchbar renders correctly with no search value', () => {
//     const tree = renderer.create(
//         <SearchBar
//             style={{}}
//             getSuggestions={jest.fn}
//             searchValue=""
//             suggestionData={[{ name: 'test1' }, { name: 'test2' }, {name: 'test3'}]}
//         />
//     ).toJSON();
//     expect(tree).toMatchSnapshot();
// });
//
// it('searchbar renders correctly with matching search value', () => {
//     const tree = renderer.create(
//         <SearchBar
//             style={{}}
//             getSuggestions={jest.fn}
//             searchValue="test1"
//             suggestionData={[{ name: 'test1' }, { name: 'test2' }, {name: 'test3'}]}
//         />
//     ).toJSON();
//     expect(tree).toMatchSnapshot();
// });