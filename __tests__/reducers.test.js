import React from 'react';
import 'react-native';
import GetTweetsReducer from '../src/reducers/GetTweetsReducer';
import CloudReducer from '../src/reducers/CloudReducer';
import reducer from '../src/reducers/';

let state = {};

it('check that no action works', () => {
    state = GetTweetsReducer({ searchValue: '', suggestions: [] }, {type: '', data: ''});
    expect(state).toEqual({ searchValue: '', suggestions: [] });
});

it('check that get suggestions action works', () => {
    state = GetTweetsReducer(state, {type: 'get_suggestions', data: [{ name: 'test1' }, { name: 'test2' }]});
    expect(state).toEqual({ searchValue: '', suggestions: [{ name: 'test1' }, { name: 'test2' }] });
});

it('check that set query action works', () => {
    state = GetTweetsReducer(state, {type: 'set_query', data: 'test' });
    expect(state).toEqual({ searchValue: 'test', suggestions: [{ name: 'test1' }, { name: 'test2' }] });
});

state = {};

it('check that no action works', () => {
    state = CloudReducer({
        results: [],
        options: {
            minTagSize: 20,
            maxTagSize: 60,
            shuffleTags: true
        }
    }, {type: '', data: ''});
    expect(state).toEqual({
        results: [],
        options: {
            minTagSize: 20,
            maxTagSize: 60,
            shuffleTags: true
        }
    });
});

it('check that set tags action works', () => {
    state = CloudReducer(state, {type: 'set_tags', data: [{ value: 'test1', count: '1' }, { value: 'test2', count: 2 }]});
    expect(state).toEqual({
        results: [{ value: 'test1', count: '1' }, { value: 'test2', count: 2 }],
        options: {
            minTagSize: 20,
            maxTagSize: 60,
            shuffleTags: true
        }
    });
});

state = {};

it('check that combine reducers works', () => {
    state = reducer({
        cloud: {
            results: [],
            options: {
                minTagSize: 20,
                maxTagSize: 60,
                shuffleTags: true
            }
        },
        coords: {
            coordinates: []
        },
        tweets: {
            searchValue: '',
            suggestions: []
        }
    }, {});
    expect(state).toEqual(
        {
            cloud: {
                results: [],
                options: {
                    minTagSize: 20,
                    maxTagSize: 60,
                    shuffleTags: true
                }
            },
            coords: {
                coordinates: []
            },
            tweets: {
                searchValue: '',
                suggestions: []
            }
        }
    );
});
