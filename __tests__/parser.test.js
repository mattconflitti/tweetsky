import { parse } from '../src/processors/parser';
import { coordParse } from '../src/processors/coordParser';
import { parseTweetCards } from '../src/processors/parseTweetCards';

it('parser behaves normally with 5 strings given to it', () => {
    const strings = ['this is a test','this is a test','hello world','hello world','hello world'];
    const results = parse(strings);
    expect(results).toEqual([
        { value: 'hello', count: 3 },
        { value: 'world', count: 3 },
        { value: 'test', count: 2 }
    ]);
});

it('parser behaves normally with 5 strings given to it and set to give only top 2 results', () => {
    const strings = ['this is a test','this is a test','hello world','hello world','hello world'];
    const results = parse(strings, 2);
    expect(results).toEqual([
        { value: 'hello', count: 3 },
        { value: 'world', count: 3 }
    ]);
});

it('no results when given only filter words', () => {
    const strings = ['the', 'with', 'at', 'from', 'into', 'until', 'against'];
    const results = parse(strings);
    expect(results).toEqual([]);
});

it('no results when no strings given', () => {
    const strings = [];
    const results = parse(strings);
    expect(results).toEqual([]);
});

it('removes punctuation', () => {
    const strings = ['hello,', 'hello.', 'hello!', 'hello?'];
    const results = parse(strings);
    expect(results).toEqual([
        { value: 'hello', count: 4 }
    ]);
});

it('removes links', () => {
    const strings = ['hello world', 'helloworld.com', 'http://www.google.com', 'a link: https://www.secure.com/'];
    const results = parse(strings);
    expect(results).toEqual([
        { value: 'hello', count: 1 },
        { value: 'world', count: 1 },
        { value: 'helloworldcom', count: 1 },
        { value: 'link', count: 1 }
    ]);
});


it('creates 10 points for map', () => {
    const coords = coordParse(10);
    expect(coords.length).toEqual(10);
});


it('properly parse 1 tweet card', () => {
    const sampleData = [
        {
            user: {
                name: 'name',
                screen_name: 'screenName',
                profile_image_url: 'img.png'
            },
            text: 'Test',
            created_at: 'On a tuesday'
        }
    ];
    const result = parseTweetCards(sampleData, 1);
    expect(result.length).toEqual(1);
    expect(result[0]).toEqual({
        name: 'name',
        screen_name: 'screenName',
        text: 'Test',
        profile_image_url: 'img.png',
        created_at: 'On a tuesday'
    });
});