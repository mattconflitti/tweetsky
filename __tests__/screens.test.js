import React from 'react';
import 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { SplashScreen } from '../src/screens';
import reducer from '../src/reducers';
import renderer from 'react-test-renderer';

const testStore = createStore(reducer, {});

// it('homescreen renders properly', () => {
//     const tree = renderer.create(
//         <Provider store={testStore}>
//             <HomeScreen minTagSize={20} maxTagSize={40} tags={[]} />
//         </Provider>
//     ).toJSON();
//     expect(tree).toMatchSnapshot();
// });

it('splash screen renders correctly', () => {
    const tree = renderer.create(
        <SplashScreen />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});