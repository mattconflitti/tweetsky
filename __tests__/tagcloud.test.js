import React from 'react';
import 'react-native';
import { TagCloud } from '../src/react-tagcloud';
import { defaultRenderer } from '../src/react-tagcloud/defaultRenderer';
import { fontSizeConverter, arraysEqual, propertiesEqual } from '../src/react-tagcloud/helpers';
import renderer from 'react-test-renderer';

it('tag cloud renders correctly', () => {
    const tree = renderer.create(
        <TagCloud
            minSize={12}
            maxSize={40}
            shuffle={false}
            disableRandomColor
            tags={[
                { value: 'hello', count: 15 },
                { value: 'world', count: 10 }
            ]}
        />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});

it('tag cloud renders correctly with no data', () => {
    const tree = renderer.create(
        <TagCloud
            minSize={12}
            maxSize={40}
            shuffle={false}
            disableRandomColor
            tags={[]}
        />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});

it('tag cloud renders correctly with on press prop set', () => {
    const tree = renderer.create(
        <TagCloud
            minSize={12}
            maxSize={40}
            shuffle={false}
            disableRandomColor
            tags={[{ value: 'hello', count: 15 },
                { value: 'world', count: 10 }]}
            onPress={() => {}}

        />
    ).toJSON();
    expect(tree).toMatchSnapshot();
});