/**
 * Parse function takes in an array of tweets and generates
 * a array of objects that contain actual tweet info
 *
 * @param tweets
 * @param num
 * @returns {Array}
 */
export const parseTweetCards = (tweets, num = 10) => {
    if (tweets.length === 0) {
        return [];
    }

    const tweetCards = [];

    for (let i = 0; i < num; i++) {
        tweetCards.push({
            name: tweets[i].user.name,
            screen_name: tweets[i].user.screen_name,
            text: tweets[i].text,
            profile_image_url: tweets[i].user.profile_image_url,
            created_at: tweets[i].created_at
        });
    }

    return tweetCards;
};
