/**
 * coordParser function takes in an array of coordinates and generates
 * a array of objects with x y parameters for the heatmap
 *
 * @param coordinateData
 * @returns {Array}
 */
export const coordParse = (coordinateDataLength) => {
    //should be a list of lists of float
    //each float is [long, lat]
    const xyCoordinates = [];

    for (let i = 0; i < coordinateDataLength; i++) {
        const long = parseInt(Math.random() * 283, 10);
        const lat = parseInt(Math.random() * 152, 10);

        // xyCoordinates.push({
        //     x: (126 + long) * 283 / 61,
        //     y: (50 - lat) * 152 / 26,
        // });

        xyCoordinates.push({ x: long, y: lat });
    }

    return xyCoordinates;
};
