import { combineReducers } from 'redux';
import GetTweetsReducer from './GetTweetsReducer';
import CloudReducer from './CloudReducer';
import HeatmapReducer from './HeatmapReducer';

/**
 * Combines all reducers needed for application
 */
const combined = combineReducers({
    tweets: GetTweetsReducer,
    cloud: CloudReducer,
    coords: HeatmapReducer
});


export default combined;
