/**
 * GetTweets reducer is used to process the actions
 * dispatched to it. When a new search returns results
 * or suggestions are given to the app from the API,
 * this function adds them to the state of the app for use
 * by the components connected to it.
 *
 * @param state
 * @param action
 * @returns {*}
 */
const getTweetsReducer = (state = { searchValue: '', suggestions: [], tweetCards: [] }, action) => {
    switch (action.type) {
        case 'get_suggestions':
            return {
                ...state,
                suggestions: action.data
            };
        case 'set_query':
            return {
                ...state,
                searchValue: action.data
            };
        case 'set_tweet_cards':
            return {
                ...state,
                tweetCards: action.data
            };
        default:
            return state;
    }
};

export default getTweetsReducer;
