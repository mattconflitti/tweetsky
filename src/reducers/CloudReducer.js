/**
 * Cloud reducer is used to process the actions
 * dispatched to it. When certain options are changed,
 * this reducer will handle it.
 *
 * @param state
 * @param action
 * @returns {*}
 */
const CloudReducer = (
    state = {
        results: [],
        options: {
            minTagSize: 20,
            maxTagSize: 60,
            shuffleTags: true
        }
    }, action) => {
    switch (action.type) {
        //TODO to be implemented next release
        // case 'set_minTagSize':
        //     return {
        //         ...state,
        //         options: {
        //             ...state.options,
        //             minTagSize: action.data
        //         }
        //     };
        // case 'set_maxTagSize':
        //     return {
        //         ...state,
        //         options: {
        //             ...state.options,
        //             maxTagSize: action.data
        //         }
        //     };
        // case 'set_shuffleTags':
        //     return {
        //         ...state,
        //         options: {
        //             ...state.options,
        //             shuffleTags: action.data
        //         }
        //     };
        case 'set_tags':
            return {
                ...state,
                results: action.data
            };
        default:
            return state;
    }
};

export default CloudReducer;
