/**
 * heatmapReducer reducer is used to process the actions
 * dispatched to it. When a new search returns results
 * or suggestions are given to the app from the API,
 * this function adds them to the state of the app for use
 * by the components connected to it.
 *
 * @param state
 * @param action
 * @returns {*}
 */
const heatmapReducer = (state = { coordinates: [] }, action) => {
    switch (action.type) {
        case 'populate_heatmap':
            return {
                ...state,
                coordinates: action.data
            };
        default:
            return state;
    }
};

export default heatmapReducer;
