import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Text,
    TouchableOpacity,
    Keyboard
} from 'react-native';
import Autocomplete from 'react-native-autocomplete-input';
import { Button } from 'native-base';

/**
 * SearchBar component serves as entry into
 * the search functionality of the application
 */
class SearchBar extends Component {

    /**
     * Set up function to get data when component is mounted to screen
     * @override
     */
    componentWillMount() {
        this.props.getSuggestions();
    }

    /**
     *
     * @param {string} query
     * @returns {Object[]}
     */
    getResults(query) {
        if (query === '') {
            return [];
        }

        const results = this.props.suggestionData;
        const regex = new RegExp(`${query.trim()}`, 'i');
        return results.filter(result => result.name.search(regex) >= 0);
    }

    /**
     * Renders the SearchBar component
     * @returns {XML}
     * @override
     */
    render() {
        const searchValue = this.props.searchValue;
        const results = this.getResults(searchValue);
        const comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
        return (
                <View style={styles.containerStyle}>
                    <View style={{ flex: 1 }}>
                        <Autocomplete
                            autoCapitalize="none"
                            autoCorrect={false}
                            containerStyle={styles.autocompleteContainer}
                            data={results.length === 1 &&
                                comp(searchValue, results[0].name) ? [] : results}
                            defaultValue={searchValue}
                            onChangeText={text => this.props.setQuery(text)}
                            placeholder="Enter a #hashtag, phrase, or @user..."
                            renderItem={({ name }) => (
                            <TouchableOpacity
                                onPress={() => {
                                        this.props.setQuery(name, true);
                                        Keyboard.dismiss();
                                    }
                                }
                            >
                              <Text style={styles.itemText}>
                                {name}
                              </Text>
                            </TouchableOpacity>
                          )}
                        />
                    </View>
                    <View style={styles.searchButton}>
                        <Button
                            style={{ backgroundColor: '#1dcaff', height: 43, width: 70 }}
                            title="Search"
                            onPress={() => {
                                Keyboard.dismiss();
                                this.props.getTweets(this.props.searchValue);
                            }}
                        >
                            Go
                        </Button>
                    </View>
                </View>
        );
    }
}

/**
 * Stylesheet contains styles for this component
 * @type {Object}
 */
const styles = StyleSheet.create({
    containerStyle: {
        flexDirection: 'row'
    },
    autocompleteContainer: {
        left: 0,
        position: 'absolute',
        right: 0,
        top: 0,
        zIndex: 10,
        flex: 1
    },
    itemText: {
        fontSize: 15,
        margin: 2,
        backgroundColor: 'white'
    },
    searchButton: {
        width: 75,
        height: 50
    }
});

export { SearchBar };
