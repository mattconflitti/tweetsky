import React from 'react';
import { View } from 'react-native';

import Heatmap from './Heatmap/Heatmap';

/**
 * Heatmap wrapper to fit into our application properly
 * @returns {XML}
 * @constructor
 */
export default function Index() {
    return (
        <View
            style={{
        backgroundColor: 'black',
        flex: 1
      }}
        >
          <Heatmap />
        </View>
    );
}