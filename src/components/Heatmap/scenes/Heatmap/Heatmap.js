import React, { Component } from 'react';
import { Platform, View, WebView } from 'react-native';
import styles from './Heatmap.styles';
import { coordParse } from '../../../../processors/coordParser';

export default class Heatmap extends Component {

    /**
     * Constructor
     */
    constructor() {
        super();
        this.state = {};
    }

    /**
     * Lifecycle method executed when component
     * is about to mount to app
     */
    componentWillMount() {
        this.populate(this.props);
    }

    /**
     * Lifecycle method called when component is mounted to screen
     */
    componentDidMount() {
        setTimeout(
            () => {
                // https://github.com/facebook/react-native/issues/953
                this.refs.heatmap.measure((ox, oy, width, height) => {
                    const radius = Math.round(width * 0.05);
                    this.handleShuffle();
                    this.setState({ width, height, radius });
                });
            }
        );
    }

    /**
     * Lifecycle method called when new props are given
     * to the component
     * @param newProps
     */
    componentWillReceiveProps(newProps) {
        this.populate(newProps);
    }

    /**
     * Gets data, generates colors, converts font sizes, and shuffles
     * the order of the words randomly.
     *
     * @param props
     */
    populate() {
        this.handleShuffle();
    }

    handleShuffle() {
        setTimeout(() => {
            const processedPoints = coordParse(30);
            this.setState({ processedPoints });
        });

        // Forcing heatmap to empty
        this.setState({ processedPoints: null });
    }

    render() {
        let webview = null;

        if (this.state.processedPoints) {
            const uri = Platform.OS === 'ios' ? 'heatmap.html' : 'file:///android_asset/heatmap.html';
            const maxValue = Math.max(...this.state.processedPoints.map((p) => p.value));
            const script = heatmapInputGenerator(this.state.processedPoints, this.state.radius, maxValue);
            webview = (<WebView
                source={{ uri }}
                scrollEnabled={false}
                injectedJavaScript={script}
                javaScriptEnabled
            />);
        }

        return (
            <View style={styles.container} >
                <View
                    style={styles.webview}
                    ref='heatmap'
                >
                    { webview }
                </View>
            </View>
        );
    }
}

const heatmapInputGenerator = (points, radius, max) => {
    return `
    var heatmapInstance = h337.create({
      container: document.querySelector('.heatmap'),
      radius: ${radius}
    });
    heatmapInstance.setData({
      max: ${max},
      data: ${JSON.stringify(points)}
    });
  `;
};
