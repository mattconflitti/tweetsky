import axios from 'axios';
import { parse } from '../processors/parser';
import { parseTweetCards } from '../processors/parseTweetCards';

/**
 * ActionCreator used to asynchronously get top
 * 50 search queries and then dispatch data to store when
 * call returns.
 * @param dispatch
 */
export const getSuggestions = (dispatch) => {
    axios.get('https://api.twitter.com/1.1/trends/place.json?id=23424977',
        { headers:
            {
                Authorization: 'Bearer AAAAAAAAAAAAAAAAAAAAALPeywAAAAAA%2B7ry%2BdaSbD7EQQbgQKqYCfpRyck%3DDyuoFY5hN4KqTZNIIX6P3L9IBdvhiZdMT6xYvoyHaVxrC6mGoJ'
            }
        })
        .then(response => {
            console.log();
            dispatch({
                type: 'get_suggestions',
                data: response.data[0].trends
            });
        })
        .catch((error) => {
            console.log(error);
        });
};

/**
 * ActionCreator does TwitterAPI call to search for given query
 * and then dispatches results to Redux store upon response.
 *
 * @param query
 * @param dispatch
 * @param count
 */
export const getTweets = (query, dispatch, count = 100) => {
    let q = '';
    let type = 'string';
    switch (query.charAt(0)) {
        case '#':
            q = encodeURIComponent(query);
            type = 'hashtag';
            break;
        case '@':
            q = query.substr(1);
            type = 'handle';
            break;
        default:
            q = query;
    }

    let endpoint = '';
    q = q.toLowerCase();
    if (type === 'string' || type === 'hashtag') {
        endpoint = `https://api.twitter.com/1.1/search/tweets.json?q=${q}&result_type=mixed&count=${count}`;
    } else {
        endpoint = `https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=${q}&count=${count}`;
    }

    axios.get(endpoint,
        {
            headers:
                {
                    Authorization: 'Bearer AAAAAAAAAAAAAAAAAAAAALPeywAAAAAA%2B7ry%2BdaSbD7EQQbgQKqYCfpRyck%3DDyuoFY5hN4KqTZNIIX6P3L9IBdvhiZdMT6xYvoyHaVxrC6mGoJ'
                }
        })
        .then(response => {
            console.log(response.data);
            //extract just tweet and concat to one array.
            let tweets = (type === 'handle') ? response.data : response.data.statuses;
            console.log(tweets);
            const tweetCards = parseTweetCards(tweets);

            dispatch({
                type: 'set_tweet_cards',
                data: tweetCards
            });

            tweets = concatTweets(tweets);

            //use parser on tweets to return [{value: '', count: 0}]
            tweets = parse(tweets);
            console.log(tweets);
            if (tweets.length > 0) {
                dispatch({
                    type: 'set_tags',
                    data: tweets
                });
            } else {
                dispatch({
                    type: 'set_tags',
                    data: [{ value: 'No tags were found!', count: 50 }]
                });
            }

            // let coordinateData = (type === 'handle') ? response.data : response.data.statuses;
            // coordinateData = pullCoordinates(coordinateData);
            // coordinateData = coordParse(coordinateData);
            // if (coordinateData.length > 0) {
            //     dispatch({
            //         type: 'populate_heatmap',
            //         data: coordinateData
            //     })
            // }
        })
        .catch((error) => {
            console.log(error);
            dispatch({
                type: 'set_tags',
                data: [{ value: 'Error occurred while trying to get tweets!', count: 50 }]
            });
        });
};

/**
 * Helper function to pull tweet coordinates only
 * into a single array
 *
 * @param coordinateData
 * @returns {Array}
 */
const pullCoordinates = (coordinateData) => {
    const cordArray = [];
    for (let i = 0; i < coordinateData.length; i++) {
        if (coordinateData[i].Coordinates !== null){
            cordArray.push(coordinateData[i].Coordinates.coordinates);
        }
    }
    return cordArray;
};

/**
 * Helper function to concatenate tweet text only
 * into a single array
 *
 * @param tweets
 * @returns {Array}
 */
const concatTweets = (tweets) => {
    const concat = [];
    for (let i = 0; i < tweets.length; i++) {
        concat.push(tweets[i].text);
    }
    return concat;
};

/**
 * Sets current query string into state
 *
 * @param query
 * @param search
 * @param dispatch
 */
export const setQuery = (query, search, dispatch) => {
    dispatch({
        type: 'set_query',
        data: query
    });
    if (search) {
        getTweets(query, dispatch);
    }
};

/**
 * Creates action to set tags into tag cloud
 * @param results
 * @param dispatch
 */
export const setTags = (results, dispatch) => {
    dispatch({
        type: 'set_tags',
        data: results
    });
};
