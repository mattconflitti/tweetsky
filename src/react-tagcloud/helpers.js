/**
 * Creates new object from target excluding given properties.
 *
 * @param target
 * @param props
 * @returns {*}
 */
export const omitProps = (target, props) => {
  return Object.keys(target).reduce((r, key) => {
    if (!~props.indexOf(key)) {
      r[key] = target[key];
    }
    return r;
  }, {});
};

/**
 * Creates new object from target including all available properties.
 *
 * @param target
 * @param props
 * @returns {*}
 */
export const includeProps = (target, props) => {
  return Object.keys(target).reduce((r, key) => {
    if (~props.indexOf(key) && key in target) {
      r[key] = target[key];
    }
    return r;
  }, {});
};

/**
 * Computes appropriate font size of tag.
 *
 * @param count
 * @param min
 * @param max
 * @param minSize
 * @param maxSize
 * @returns {number}
 */
export const fontSizeConverter = (count, min, max, minSize, maxSize) => {
  return Math.round((count - min) * (maxSize - minSize) / (max - min) + minSize);
};

/**
 * Returns true if arrays contains the same elements.
 *
 * @param arr1
 * @param arr2
 * @returns {boolean}
 */
export const arraysEqual = (arr1, arr2) => {
  if (arr1.length !== arr2.length) {
    return false;
  }
  return arr1.every((o, i) => o === arr2[i]);
};

/**
 * Standard equals function to check if two tags' properties are equal.
 *
 * @param o1
 * @param o2
 * @param properties
 * @returns {boolean}
 */
export const propertiesEqual = (o1, o2, properties) => {
  return properties.every(prop => o1[prop] === o2[prop]);
};
