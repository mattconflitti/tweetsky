import React, { Component } from 'react';
import { Text } from 'react-native';
import arrayShuffle from 'array-shuffle';
import randomColor from 'randomcolor';
import { defaultRenderer } from './defaultRenderer';
import {
    omitProps,
    includeProps,
    fontSizeConverter,
    arraysEqual,
    propertiesEqual } from './helpers';

const eventHandlers = ['onPress'];
const cloudProps = [
    'tags',
    'shuffle',
    'renderer',
    'maxSize',
    'minSize',
    'colorOptions',
    'disableRandomColor'
];

/**
 * Uses randcomcolor package to generate a random color.
 * It will use color given if it exists and no color if disabled.
 *
 * @param tag
 * @param disableRandomColor
 * @param colorOptions
 * @returns {*}
 */
const generateColor = (tag, { disableRandomColor, colorOptions }) => {
    if (tag.color) {
        return tag.color;
    }
    if (disableRandomColor) {
        return undefined;
    }
    var colors = ['#0084b4', '#00aced', '#1dcaff', '#c0deed', '#1dfffc'];
    return colors[Math.floor(Math.random() * 5)];
};

/**
 * TagCloud creates a block of randomly colored text tags
 * that are created from a list of words with their
 * corresponding count from whichever source devs used.
 */
export class TagCloud extends Component {

    /**
     * Lifecycle method executed when component
     * is about to mount to app
     */
    componentWillMount() {
        this.populate(this.props);
    }

    /**
     * Lifecycle method called when new props are given
     * to the component
     * @param newProps
     */
    componentWillReceiveProps(newProps) {
        const propsEqual = propertiesEqual(this.props, newProps, Object.keys(TagCloud.propTypes));
        const tagsEqual = arraysEqual(newProps.tags, this.props.tags);
        if (!tagsEqual || !propsEqual) {
            this.populate(newProps);
        }
    }

    /**
     * Generates a text tag and attaches event handlers
     * to them.
     *
     * @returns {Array[*]}
     */
    attachEventHandlers() {
        const cloudHandlers = includeProps(this.props, eventHandlers);
        return this.data.map(({ tag, fontSize, color }) => {
            const elem = this.props.renderer(tag, fontSize, color);
            const tagHandlers = includeProps(elem.props, eventHandlers);
            const globalHandlers = Object.keys(cloudHandlers).reduce((r, k) => {
                r[k] = e => {
                    cloudHandlers[k](tag, e);
                    tagHandlers[k] && tagHandlers(e);
                };
                return r;
            }, {});
            return React.cloneElement(elem, globalHandlers);
        });
    }

    /**
     * Gets data, generates colors, converts font sizes, and shuffles
     * the order of the words randomly.
     *
     * @param props
     */
    populate(props) {
        const { tags, shuffle, minSize, maxSize } = props;
        const counts = tags.map(tag => tag.count);
        const min = Math.min(...counts);
        const max = Math.max(...counts);
        const data = tags.map(tag => ({
            tag,
            color: generateColor(tag, props),
            fontSize: fontSizeConverter(tag.count, min, max, minSize, maxSize)
        }));
        //shuffle data is specified to
        this.data = shuffle ? arrayShuffle(data) : data;
    }

    /**
     * Renders the tag cloud to the screen
     * @returns {XML}
     */
    render() {
        const props = omitProps(this.props, [...cloudProps, ...eventHandlers]);
        const tagElements = this.attachEventHandlers();
        return (
            <Text {...props}>
                {' '}{ tagElements }{' '}
            </Text>
        );
    }

}

/**
 * Defines the proptypes explicitly for
 * app regularity
 */
TagCloud.propTypes = {
    tags: React.PropTypes.array.isRequired,
    maxSize: React.PropTypes.number.isRequired,
    minSize: React.PropTypes.number.isRequired,
    shuffle: React.PropTypes.bool,
    colorOptions: React.PropTypes.object,
    disableRandomColor: React.PropTypes.bool,
    renderer: React.PropTypes.func
};

/**
 * Object contains the defaults of the given
 * properties just in case dev never defines
 * them during use.
 */
TagCloud.defaultProps = {
    renderer: defaultRenderer,
    shuffle: true,
    colorOptions: {}
};

//TODO add options for colors in settings.
