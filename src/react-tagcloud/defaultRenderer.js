import React from 'react';
import { Text } from 'react-native';

/**
 * This function is the default renderer for the tag cloud.
 * Allows customization of the size and color of the tag.
 *
 * @param tag {Object}
 * @param size {Number}
 * @param color {string}
 * @returns {XML}
 */
export const defaultRenderer = (tag, size, color) => {
  const fontSize = size;
  const key = tag.key || tag.value;
  const style = { color, fontSize, ...styles };
  return <Text style={style} key={key}>{tag.value}</Text>;
};

/**
 * Basic styles for tags.
 */
const styles = {
    marginLeft: 3,
    marginRight: 3
};
