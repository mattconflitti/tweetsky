import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Image } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem, Grid, Col, Row } from 'native-base';
import dateformat from 'dateformat';
import SearchBarContainer from '../containers/SearchBarContainer';
import { TagCloud } from '../react-tagcloud/';
import * as actions from '../actions';
import Heatmap from '../components/Heatmap/scenes';

/**
 * HomeScreen is the main screen of the app. It will contain the SearchBar
 * and it will display the generated tagcloud.
 */
export class HomeScreen extends Component {

    /**
     * Constructor method used to set state
     */
    constructor() {
        super();
        this.state = {
            active: false,
            showTweets: false
        };
    }

    /**
     * Renders the home screen component
     * @returns {XML}
     */
    render() {
        return (

            <Container style={{ flex: 1, position: 'relative' }}>
                <Content>
                    <Card>
                        <CardItem>
                            <SearchBarContainer style={{ position: 'absolute' }} />
                        </CardItem>
                        <CardItem>
                    <Container
                        style={{
                            flex: 1,
                            flexWrap: 'wrap',
                            height: 500,
                            marginTop: 20,
                            justifyContent: 'center',
                            alignItems: 'center'
                        }}
                    >
                        <Content>

                            <TagCloud
                                style={{ flex: 1, flexDirection: 'row' }}
                                minSize={this.props.minTagSize}
                                maxSize={this.props.maxTagSize}
                                shuffle={this.props.shuffleTags}
                                tags={this.props.tags}
                                onPress={(tag) => {
                                    console.log(tag);
                                    this.props.setQuery(tag.value, true);
                                }}
                            />
                        </Content>
                    </Container>
                        </CardItem>
                        <CardItem>
                            <View
                                style={{
                                    flex: 1,
                                    flexWrap: 'wrap',
                                    marginTop: 20,
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    backgroundColor: '#36454f'
                                }}
                            >
                            <Heatmap />
                            </View>
                        </CardItem>
                        <CardItem>
                            <View>
                                <Button
                                    style={{ alignSelf: 'center' }}
                                    onPress={() => {
                                        this.setState({ showTweets: !this.state.showTweets });
                                    }}
                                >
                                    <Text
                                        style={{
                                            color: 'white'
                                    }}
                                    >
                                        {!this.state.showTweets ? 'Show Tweets' : 'Hide Tweets'}
                                    </Text>
                                </Button>
                            </View>
                        </CardItem>
                        {/* This self calling function loops through
                        tweet info and creates DOM to display it */}
                        {((show) => {
                            if (show) {
                                const tweets = this.props.tweetCards;
                                return tweets.map((tweet) =>
                                    (
                                        <CardItem
                                            key={`${tweet.created_at} ${parseInt(Math.random() * 100, 10)}`}
                                        >
                                            <Grid>
                                                <Row>
                                                    <Col style={{ width: 115 }}>
                                                        <Image
                                                        style={{ width: 100, height: 100 }}
                                                        source={{ uri: tweet.profile_image_url }}
                                                        />
                                                    </Col>
                                                    <Col>
                                                        <Row style={{ flexDirection: 'column' }}>
                                                            <Text style={{ fontSize: 20 }}>
                                                                {tweet.name}
                                                            </Text>
                                                            <Text style={{ fontSize: 12 }}>
                                                                @{tweet.screen_name}
                                                            </Text>
                                                            <Text style={{ fontSize: 12 }}>
                                                                {dateformat(new Date(tweet.created_at), 'dddd, mmmm dS, yyyy, h:MM:ss TT')}
                                                            </Text>
                                                        </Row>
                                                    </Col>
                                                </Row>
                                                <Row><Text>{tweet.text}</Text></Row>
                                            </Grid>
                                        </CardItem>
                                    )
                                );
                            }

                            return null;
                        })(this.state.showTweets)}
                    </Card>
                </Content>
            </Container>
        );
    }
}

/**
 * Maps state of Redux store to props of the HomeScreen component
 *
 * @param state
 * @returns {{tags: Array}}
 */
const mapStateToProps = (state) => {
    return {
        tags: state.cloud.results,
        minTagSize: state.cloud.options.minTagSize,
        maxTagSize: state.cloud.options.maxTagSize,
        shuffleTags: state.cloud.options.shuffleTags,
        tweetCards: state.tweets.tweetCards
    };
};

/**
 * Maps dispatch functions to props so that TagCloud can dispatch events
 * to manipulate the store of the app
 *
 * @param dispatch
 * @returns {{setQuery: (function(*=): *)}}
 */
function mapDispatchToProps(dispatch) {
    return {
        setQuery: (query, search = false) => actions.setQuery(query, search, dispatch)
    };
}

/**
 * Connect the component to the app's state
 */
const HomeScreenContainer = connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

export { HomeScreenContainer };
