import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Image
} from 'react-native';


/**
 * Splash screen to show TweetSky logo upon loading application
 */
class SplashScreen extends Component {
    /**
     * Lifecycle method that executes once the component mounts to the app.
     * Starts timeout so the screen is only shown for desired amount of time
     */
    componentDidMount() {
        setTimeout(() => {
            this.props.navigator.replace({
                title: 'TweetSky',
                index: 0,
                name: 'home'
            });
        }, 2000);
    }

    /**
     * Renders component, places TweetSky logo on screen
     * @returns {XML}
     */
    render() {
        return (
            <View style={styles.container}>
                <Image
                    style={styles.image}
                    source={require('../assets/logo.png')}
                />
            </View>
        );
    }
}

/**
 * Object contains styles for this screen.
 */
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'white'
    },
    image: {
        flex: 1,
        width: 200,
        height: 200,
        resizeMode: 'contain'
    }
});

export { SplashScreen };
