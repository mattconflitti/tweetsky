import { connect } from 'react-redux';
import { SearchBar } from '../components';
import * as actions from '../actions';

/**
 * Maps state of Redux store to props of the SearchBar component
 *
 * @param state
 * @returns {{suggestionData: (Array|*), tweetData: (Array|*)}}
 */
const mapStateToProps = (state) => {
    return {
        suggestionData: state.tweets.suggestions,
        searchValue: state.tweets.searchValue
    };
};

/**
 * Maps dispatch functions to props so that SearchBar can dispatch events
 * to manipulate the store of the app
 *
 * @param dispatch
 * @returns {{getSuggestions: (function()), getTweets: (function(*=))}}
 */
function mapDispatchToProps(dispatch) {
    return {
        getSuggestions: () => actions.getSuggestions(dispatch),
        getTweets: (query) => actions.getTweets(query, dispatch),
        setQuery: (query, search = false) => actions.setQuery(query, search, dispatch),
    };
}

/**
 * Connect the component to the app's state
 */
export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
